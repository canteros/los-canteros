﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LogoMaster.Master" AutoEventWireup="true" CodeBehind="Autentifica.aspx.cs" Inherits="AplicacionWeb.Autentifica" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="codigoDiv" style="text-align:center;padding-top:30px;">
        <p style="color:white">Se ha enviado un código de confirmación a <b><asp:Label ID="labelEmail" runat="server" Text="Prueba"></asp:Label></b></p>
        <p style="color:white">(Recuerda mirar en la carpeta de spam)</p>
        <p style="color:white">Por favor, introduce el código en el cuadro de abajo:</p>
        <asp:TextBox ID="entryCodigo" runat="server" style="width:60px; text-align:center;"></asp:TextBox>
        <p><asp:Button ID="botonAutentificar" ValidationGroup="codigoGroup" runat="server" Text="Autentificar" OnClick="botonAutentificar_Click" /></p>
        <p style="color:red;"><asp:RegularExpressionValidator CssClass="errorValidation" ID="checkCodigo" runat="server" ErrorMessage="El código debe ser númerico de 4 digitos" ValidationExpression="^\d{4}" ValidationGroup="codigoGroup" ControlToValidate="entryCodigo"></asp:RegularExpressionValidator></p>
        <p style="color:red;"><asp:RequiredFieldValidator CssClass="errorValidation" ID="checkCodigo2" runat="server" ValidationGroup="codigoGroup" ErrorMessage="No has introducido el código" ControlToValidate="entryCodigo"></asp:RequiredFieldValidator></p>
        </div>
</asp:Content>
