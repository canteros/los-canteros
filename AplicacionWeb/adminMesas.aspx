﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LogoMaster.Master" AutoEventWireup="true" CodeBehind="adminMesas.aspx.cs" Inherits="AplicacionWeb.adminMesas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #altaMesa{
            position:absolute;
            left:0px;
            right:0px;
            margin-left:auto;
            margin-right:auto;
            width:400px;
            height:70px;
            top:30px;
        }
        #listaMesas{
            position:absolute;
            left:0px;
            right:0px;
            margin-left:auto;
            margin-right:auto;
            width:210px;
            height:300px;
            top:150px;
            right:300px;
            overflow-y:scroll;
            background-color:#1e3760;
            border: 2px solid black;
            color:white;
            text-align:center;
        }
        #otrosControles{
            position:absolute;
            left:0px;
            right:0px;
            margin-left:auto;
            margin-right:auto;
            width:300px;
            height:400px;
            top:150px;
            left:300px;
            text-align:center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="altaMesa">

        <b style="text-align:center; position:relative; left:150px;">Alta de Mesas:</b>
    <p></p>
        <span class="styleAlta">Numero Mesa: </span><asp:TextBox CssClass="styleAlta" style="width:30px;" ID="entryAltaNumero" runat="server"></asp:TextBox>
        <span class="styleAlta">Capacidad: </span><asp:TextBox CssClass="styleAlta" style="width:30px;" ID="entryAltaCapacidad" runat="server"></asp:TextBox>
        <asp:Button CssClass="styleAlta" ID="buttonAlta" runat="server" Text="Dar de Alta" OnClick="buttonAlta_Click" ValidationGroup="altaValidation" />
        <br><br>
        <asp:RequiredFieldValidator ID="requeridoNumero" CssClass="errorValidation" runat="server" ErrorMessage="Pon un numero de mesa" ValidationGroup="altaValidation" ControlToValidate="entryAltaNumero"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="requridoCapcidad" CssClass="errorValidation" runat="server" ErrorMessage="Pon una capacidad" ControlToValidate="entryAltaCapacidad" ValidationGroup="altaValidation"></asp:RequiredFieldValidator>
    </div>

    <div id="listaMesas">
        <b>Lista de mesas</b>
        <asp:Table runat="server" style="text-align:left;" ID="tablaMesas"></asp:Table>
    </div>

    <div id="otrosControles">
        <asp:Panel ID="panelControles" runat="server">
        <b>Dar de baja:</b>
        <br><br>
        Mesa: <asp:DropDownList ID="listEliminar" runat="server"></asp:DropDownList>
        <asp:Button ID="buttonEliminar" runat="server" Text="Eliminar Mesa" OnClick="buttonEliminar_Click" />
        <br><br><br><br><br>

        
        <b>Modificar capacidad:</b>
        <br><br>
        Mesa: <asp:DropDownList ID="listModificar" runat="server"></asp:DropDownList> Capacidad: <asp:TextBox ID="entryModificarCapacidad" style="width:30px;" runat="server"></asp:TextBox>
            <p></p>
            <asp:Button ID="buttonModificar" runat="server" Text="Modificar mesa" ValidationGroup="modificarGroup" OnClick="buttonModificar_Click" />
            <br><br>
            <asp:RequiredFieldValidator CssClass="errorValidation" ID="requerirCapacidad" runat="server" ErrorMessage="Introduce una capacidad" ControlToValidate="entryModificarCapacidad" ValidationGroup="modificarGroup"></asp:RequiredFieldValidator>
            <div style="
                position:absolute;
                left:-255px;">

                <br><br><br><br>
             
          <font face="Calibri", size="4", color=" Red"> <b><ins>AVISO </color></ins> :</b></font>
          <font face="Calibri", size="4", color=" BLACK"> <b> Si borras una mesa borrarás las reservas asociadas a esa mesa</b></font>

            </div>
            </asp:Panel>
        </div>
</asp:Content>
