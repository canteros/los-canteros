﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using CanterosLibrary;

namespace AplicacionWeb
{
    public partial class Productos : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (panelBotones != null)
            {
                if (Session["Usuario"] == null) panelBotones.Visible = false;
                else panelBotones.Visible = true;
                if (labelOculto.Text == "") panelBotones.Visible = false;
                esFav.Visible = false;
                
            }


            
            ProductoCAD cad = new ProductoCAD();
            ArrayList productos = cad.todoProductos();
            for (int i = 0; i < productos.Count; i++)
            {
                Producto p = (Producto)productos[i];
                ImageButton b = new ImageButton();
                b.CssClass = "botonesProductos";
                b.ImageUrl = "/productos/" + p.IdProducto + ".jpg";
                b.ID = p.IdProducto;
                b.Click += b_Click;
                if (botonesProductos != null) botonesProductos.Controls.Add(b);
            }

        }

        void b_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton b = (ImageButton)sender;
            ProductoCAD cad = new ProductoCAD();
            Producto p = cad.dameProducto(b.ID);
            imagenProducto.ImageUrl = "/productos/" + p.IdProducto + ".jpg";
            labelOculto.Text = p.IdProducto;
            labelNombre.Text = p.NombreProducto;
            labelPrecio.Text = p.PrecioProducto.ToString("0.00") + " €";
            if (Session["Usuario"] == null) {
                panelBotones.Visible = false;
                esFav.Visible = false;
            }
            else
            {
                Usuario u = (Usuario)Session["Usuario"];
                panelBotones.Visible = true;
                string favorito = cad.getFav(u.Email);
                if (favorito == b.ID) esFav.Visible = true;
            }
        }

        protected void buttonFavorito_Click(object sender, EventArgs e)
        {
            Usuario u = (Usuario)Session["Usuario"];
            ProductoCAD cad = new ProductoCAD();
            if (cad.altaFav(labelOculto.Text, u.Email))
            {
                Session.Add("Alert", "Has elegido tu producto favorito con éxito");
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                Session.Add("Alert", "Ha ocurrido un error al seleccionar el producto, vuelvelo a intentar por favor");
                Response.Redirect("Inicio.aspx");
            }
        }
    }
}