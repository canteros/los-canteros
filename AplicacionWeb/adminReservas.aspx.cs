﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CanterosLibrary;
using System.Collections;


namespace AplicacionWeb
{
    public partial class adminReservas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Usuario"] == null)
            {
                Session.Add("Alert", "No tienes permiso para acceder a esta página");
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                Usuario u = (Usuario)Session["Usuario"];
                UsuarioCAD ucad = new UsuarioCAD();
                if (ucad.getTipo(u.Email) != "Admin")
                {
                    Session.Add("Alert", "No tienes permiso para acceder a esta página");
                    Response.Redirect("Inicio.aspx");
                }
            }



            ReservaCAD cad = new ReservaCAD();
            ArrayList todo = cad.todoReservas();
            ArrayList definitivo = new ArrayList();
            for (int i = 0; i < todo.Count; i++)
            {
                CanterosLibrary.Reservas r = (CanterosLibrary.Reservas)todo[i];
                DateTime fecha = DateTime.Now.AddDays(-1);
                if (fecha < r.DiaReserva) definitivo.Add(r);
            }

            if (definitivo.Count <= 0) inf.Visible = true;
            else
            {
                inf.Visible = false;
                TableRow row = new TableRow();
                TableCell c1 = new TableCell();
                TableCell c2 = new TableCell();
                TableCell c3 = new TableCell();
                TableCell c4 = new TableCell();
                TableCell c5 = new TableCell();

                c1.CssClass = "tablaReservas";
                c2.CssClass = "tablaReservas";
                c3.CssClass = "tablaReservas";
                c4.CssClass = "tablaReservas";
                c5.CssClass = "tablaReservas";

                c1.Text = "Dia";
                c2.Text = "Usuario";
                c3.Text = "Mesa";
                c4.Text = "Camarero";
                c5.Text = "Fotografo";

                row.Cells.Add(c1);
                row.Cells.Add(c2);
                row.Cells.Add(c3);
                row.Cells.Add(c4);
                row.Cells.Add(c5);

                tablaReservas.Rows.Add(row);

                for (int i = 0; i < definitivo.Count; i++)
                {
                    CanterosLibrary.Reservas res = (CanterosLibrary.Reservas)definitivo[i];

                    row = new TableRow();
                    c1 = new TableCell();
                    c2 = new TableCell();
                    c3 = new TableCell();
                    c4 = new TableCell();
                    c5 = new TableCell();

                    c1.CssClass = "tablaReservas";
                    c2.CssClass = "tablaReservas";
                    c3.CssClass = "tablaReservas";
                    c4.CssClass = "tablaReservas";
                    c5.CssClass = "tablaReservas";


                    DateTime fe = res.DiaReserva;
                    string cam = "No", fot = "No";
                    if (res.CamareroPrivadoReserva) cam = "Sí";
                    if (res.FotorgafoReserva) fot = "Sí";

                    c1.Text = fe.Day+"/"+fe.Month+"/"+fe.Year;
                    c2.Text = res.Usuario;
                    c3.Text = res.Mesa.ToString();
                    c4.Text = cam;
                    c5.Text = fot;

                    row.Cells.Add(c1);
                    row.Cells.Add(c2);
                    row.Cells.Add(c3);
                    row.Cells.Add(c4);
                    row.Cells.Add(c5);

                    tablaReservas.Rows.Add(row);
                }
            }

        }
    }
}