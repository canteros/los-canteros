﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CanterosLibrary;
using System.Collections;
using System.IO;

namespace AplicacionWeb
{
    public partial class Galeria : System.Web.UI.Page
    {
        ArrayList fotos;


        protected void Page_Load(object sender, EventArgs e)
        {
            buttonEliminar.Visible = false;
            panelValoracion.Visible = false;
            labelAviso.Visible = false;
            FotoCAD cad = new FotoCAD();
            fotos = cad.todoFotos();
            for (int i = 0; i < fotos.Count; i++)
            {
                Foto f = new Foto();
                f = (Foto)fotos[i];
                ImageButton b = new ImageButton();
                b.ID = f.Path;
                string carpeta = "~/usersgalery/";
                b.ImageUrl = carpeta + f.Path + ".jpg";
                panelBotonesGaleria.Controls.Add(b);
                b.Click += new ImageClickEventHandler(ButtonImage_handler);
                //b.Width = 150;
                b.CssClass = "fotostyle";
            }
        }

        protected void ButtonImage_handler(object sender, EventArgs e)
        {
            ImageButton b = (ImageButton)sender;
            imagenSeleccionada.ImageUrl = "~/usersgalery/"+b.ID + ".jpg";
            labelOculto.Text = b.ID;
            Foto f = new Foto();
            FotoCAD cad = new FotoCAD();
            f = cad.dameFoto(b.ID);
            if (f.Path == "")
            {
                Session.Add("Alert", "Error al recuperar datos de la foto");
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                labelNombre.Text = f.Name;
                labelDescripcion.Text = f.Description;
                labelFecha.Text = f.UploadDate.Day + "/" + f.UploadDate.Month + "/" + f.UploadDate.Year;
                Usuario u = new Usuario();
                UsuarioCAD ucad = new UsuarioCAD();
                u = ucad.getUsuario(cad.uploaderFoto(b.ID));
                labelUploader.Text = u.Nombre;
                int votos = cad.numeroVotos(b.ID);
                if (votos <= 0)
                {
                    labelVotos.Text = "No hay votos";
                    labelMedia.Text = "No hay votos";
                }
                else
                {
                    labelMedia.Text = cad.mediaVotos(b.ID).ToString("0.00")+"/5";
                    labelVotos.Text = cad.numeroVotos(b.ID).ToString();
                }
                if (Session["Usuario"] == null)
                {
                    panelValoracion.Visible = false;
                    labelAviso.Visible = true;
                }
                else
                {
                    panelValoracion.Visible = true;
                    labelAviso.Visible = false;
                }
                if (Session["Usuario"] != null)
                {
                    Usuario actual = (Usuario)Session["Usuario"];
                    if (actual.Email == u.Email) buttonEliminar.Visible = true;
                    else if (ucad.getTipo(actual.Email) == "Admin") buttonEliminar.Visible = true;
                }
            }
        }

        protected void buttonValorar_Click(object sender, EventArgs e)
        {
            Usuario u = (Usuario)Session["Usuario"];
            FotoCAD cad = new FotoCAD();
            cad.bajaValoracion(u.Email, labelOculto.Text);
            cad.altaValoracion(u.Email, labelOculto.Text, Convert.ToInt32(DropDownList1.SelectedValue));
            Session.Add("Alert", "Foto valorada correctamente");
            Response.Redirect("Inicio.aspx");
        }

        protected void buttonEliminar_Click(object sender, EventArgs e)
        {
            FotoCAD cad = new FotoCAD();
            if (cad.bajaFoto(labelOculto.Text))
            {
                string path = Server.MapPath("~/usersgalery/");
                File.Delete(path + labelOculto.Text + ".jpg");
                Session.Add("Alert", "Se ha eliminado la foto con éxito");
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                Session.Add("Alert", "Ha ocurrido un error, la foto no se ha podido eliminar. Vuelvelo a intentar");
                Response.Redirect("Inicio.aspx");
            }
        }
    }
}