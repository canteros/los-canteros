﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LogoMaster.Master" AutoEventWireup="true" CodeBehind="adminReservas.aspx.cs" Inherits="AplicacionWeb.adminReservas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #divLista{
            position:absolute;
            left:0px;
            right:0px;
            margin-left:auto;
            margin-right:auto;
            width:400px;
            height:70px;
            top:30px;
        }
        .tablaReservas{
            border:2px solid black;
            padding:2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divLista">
        <b style="position:absolute; left:135px;">Lista de reservas:</b>
        <br><br><br>
        <span style="position:absolute; left:105px;" id="inf" runat="server"><i>No hay reservas actualmente</i></span>
        <asp:Table ID="tablaReservas" runat="server"></asp:Table>
    </div>
</asp:Content>
