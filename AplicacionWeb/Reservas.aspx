﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LogoMaster.Master" AutoEventWireup="true" CodeBehind="Reservas.aspx.cs" Inherits="AplicacionWeb.Reservas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #divHacerReserva{
            height:250px;
            width:800px;
            position:absolute;
            left:0px;
            right:0px;
            margin-left:auto;
            margin-right:auto;
            background-color:#5573b1;
            border: 5px solid #495b80;
            border-radius:10px;
            
        }
        #divListMesas{
            position:relative;
            bottom:125px;
            right:30px;
        }
        #divCheck{
            position:relative;
            bottom:160px;
            left:480px;
        }
        #divBoton{
            position:relative;
            bottom:200px;
            left:650px;
            width:0px;
        }
        #divMostrarReservas{
            position:absolute;
            left:0px;
            right:0px;
            margin-left:auto;
            margin-right:auto;
            height:200px;
            width:600px;
            top:300px;
            border: 5px solid #495b80;
            border-radius:15px;
            overflow-y:scroll;
            background-color:#5573b1;
        }
        .calendario{
            z-index:500;
        }
        .reservasNo{
            color:white;
            position:absolute;
            left:40px;
            right:0px;
            top:80px;
            margin-left:auto;
            margin-right:auto;
        }
        .claseLabelReserva{
            color:white;
            float:left;
            padding:5px;
        }
        .claseEliminarReserva{
            color:red;
            float:right;
            padding:5px;
            padding-right:50px;
        }
        .panelEliminar{
            position:absolute;
            float:right;
            top:0px;
            right:0px;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divHacerReserva" style="color:white; float:left; text-align:center;">
        <asp:Calendar CssClass="calendario" ID="calendarioReservas" runat="server" OnSelectionChanged="calendarioReservas_SelectionChanged">
            <TitleStyle BackColor="#313d4f" Font-Bold="true" BorderStyle="Solid" BorderColor="#61a0b4" BorderWidth="2"/>
            <DayHeaderStyle BackColor="#436678" BorderStyle="Solid" BorderColor="#61a0b4" BorderWidth="2" />
            <SelectedDayStyle BackColor="#91C9DB" />
            <DayStyle ForeColor="White" BorderStyle="Solid" BorderColor="#61a0b4" BorderWidth="2" />
            <SelectedDayStyle ForeColor="Black" Font-Bold="true"/>
            <OtherMonthDayStyle ForeColor="LightGray" Font-Italic="true"/>
        </asp:Calendar>
        <div id="divListMesas">Capacidad de la mesa <asp:DropDownList ID="listMesas" runat="server"></asp:DropDownList></div>
        <div id="divCheck">
            <asp:CheckBoxList ID="checkFotografoCamarero" runat="server">
                <asp:ListItem Text="Fotógrafo" Enabled="true"></asp:ListItem>
                <asp:ListItem Text="Camarero privado" Enabled="true"></asp:ListItem>
            </asp:CheckBoxList>
        </div>
        <div id="divBoton">
            <asp:Button ID="buttonReserva" runat="server" Text="Hacer Reserva" ValidationGroup="reservaGroup" OnClick="buttonReserva_Click"/>
        </div>
        <i style="position:relative; bottom:80px;">Recuerda, pedir camarero privado o fotógrafo conllevará un extra en la reserva</i>
        <asp:RequiredFieldValidator style="position:absolute; bottom:70px; right:10px;" ID="validationMesa" runat="server" CssClass="errorValidation" ErrorMessage="Tienes que seleccionar una mesa" ControlToValidate="listMesas" ValidationGroup="reservaGroup"></asp:RequiredFieldValidator>
    </div>
    <div id="divMostrarReservas">
        <asp:Panel ID="panelReservas" runat="server">
            <asp:Panel runat="server" CssClass="panelEliminar" ID="panelEliminar">
                <asp:DropDownList ID="listEliminar" style="position:absolute; top:5px; right:20px;" runat="server"></asp:DropDownList>
                <asp:LinkButton ID="linkEliminar" style="color:#a90404; position:absolute; top:35px; right:35px;" runat="server" OnClick="linkEliminar_Click"><b>Cancelar reserva</b></asp:LinkButton>
            </asp:Panel>
        <asp:Label ID="labelNoReservas" CssClass="reservasNo" runat="server" Text="<b><u>No tienes ninguna reserva hecha. Puedes hacerlo en las opciones de arriba</u></b>"></asp:Label>
            </asp:Panel>
    </div>
</asp:Content>
