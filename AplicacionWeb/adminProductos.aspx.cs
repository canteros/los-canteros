﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CanterosLibrary;
using System.Collections;

namespace AplicacionWeb
{
    public partial class adminProductos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Usuario"] == null)
            {
                Session.Add("Alert", "No tienes permiso para acceder a esta página");
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                Usuario u = (Usuario)Session["Usuario"];
                UsuarioCAD ucad = new UsuarioCAD();
                if (ucad.getTipo(u.Email) != "Admin")
                {
                    Session.Add("Alert", "No tienes permiso para acceder a esta página");
                    Response.Redirect("Inicio.aspx");
                }
            }

            ProductoCAD cad = new ProductoCAD();
            ArrayList productos = cad.todoProductos();

            TableRow row = new TableRow();
            TableCell c1 = new TableCell();
            TableCell c2 = new TableCell();
            TableCell c3 = new TableCell();

            c1.CssClass = "tablaP";
            c2.CssClass = "tablaP";
            c3.CssClass = "tablaP";

            c1.Text = "ID";
            c2.Text = "Nombre";
            c3.Text = "Precio";

            row.Cells.Add(c1);
            row.Cells.Add(c2);
            row.Cells.Add(c3);

            tablaProductos.Rows.Add(row);

            for (int i = 0; i < productos.Count; i++)
            {
                Producto p = (Producto)productos[i];

                row = new TableRow();
                c1 = new TableCell();
                c2 = new TableCell();
                c3 = new TableCell();

                c1.CssClass = "tablaP";
                c2.CssClass = "tablaP";
                c3.CssClass = "tablaP";

                c1.Text = p.IdProducto;
                c2.Text = p.NombreProducto;
                c3.Text = p.PrecioProducto.ToString("0.00") ;

                row.Cells.Add(c1);
                row.Cells.Add(c2);
                row.Cells.Add(c3);

                ListItem item = new ListItem();
                item.Text = p.IdProducto;
                item.Value= p.IdProducto;

                listaModificar.Items.Add(item);
                listEliminar.Items.Add(item);

                tablaProductos.Rows.Add(row);
            }

        }

        protected void buttonAlta_Click(object sender, EventArgs e)
        {
            bool ok = false;
            string carpeta = Server.MapPath("~/productos/");
            string tipoImagen = System.IO.Path.GetExtension(altaFoto.FileName).ToLower();
            string[] tipos = { ".jpg", ".jpeg" };
            for (int i = 0; i < tipos.Length; i++)
            {
                if (tipos[i] == tipoImagen) ok = true;
            }

            if (!ok)
            {
                Session.Add("Alert", "Solo se admiten archivos jpg y jpeg");
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                Producto p = new Producto();
                ProductoCAD cad = new ProductoCAD();
                p.IdProducto = cad.generaId();
                p.NombreProducto = entryAltaNombre.Text;
                try
                {
                    p.PrecioProducto = Convert.ToSingle(entryAltaPrecio.Text);
                }
                catch (Exception ex)
                {
                    Session.Add("Alert", "Formato numérico del precio incorrecto (Ej '4,5' sería correcto)");
                    Response.Redirect("Inicio.aspx");
                }

                if (cad.altaProducto(p,p.IdProducto))
                {
                    try
                    {
                        if (!altaFoto.HasFile)
                        {
                            Session.Add("Alert", "No has insertado ninguna foto");
                            Response.Redirect("Inicio.aspx");
                        }
                        altaFoto.SaveAs(carpeta + p.IdProducto + ".jpg");
                    }
                    catch (Exception ex)
                    {
                        Session.Add("Alert", "Error inesperado: " + ex.Message);
                        Response.Redirect("Inicio.aspx");
                    }
                    Session.Add("Alert", "Producto dado de alta con éxito");
                    Response.Redirect("Inicio.aspx");
                }
                else
                {
                    Session.Add("Alert", "Error al subir producto, vuelvelo a intentar por favor");
                    Response.Redirect("Inicio.aspx");
                }
            }
        }

        protected void buttonEliminar_Click(object sender, EventArgs e)
        {
            ProductoCAD cad = new ProductoCAD();
            if (cad.bajaProducto(listEliminar.SelectedValue))
            {
                Response.Redirect("adminProductos.aspx");
            }
            else
            {
                Session.Add("Alert", "Error al eliminar producto, vuelvelo a intentar por favor");
                Response.Redirect("Inicio.aspx");
            }
        }

        protected void buttonModificar_Click(object sender, EventArgs e)
        {
            ProductoCAD cad = new ProductoCAD();

            float num = Convert.ToSingle(entryModificarPrecio.Text);
            Producto p = new Producto("", entryModificarNombre.Text, num);
            if (cad.modificaProducto(listaModificar.SelectedValue, p))
            {
                Response.Redirect("adminProductos.aspx");
            }
            else
            {
                Session.Add("Alert", "Error al modificar mesa, vuelvelo a intentar por favor");
                Response.Redirect("Inicio.aspx");
            }
        }




    }
}