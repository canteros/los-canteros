﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AplicacionWeb
{
    public partial class Inicio : System.Web.UI.Page
    {

        //Funcion que genera un popup con javascript, con "mensaje" como texto
        protected void PopUp(string mensaje)
        {
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message Box", "<script language='javascript'>alert('" + mensaje + "')</script>");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Alert"] != null)
            {
                PopUp(Session["Alert"].ToString());
                Session.Remove("Alert");
            }
        }
    }
}