﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CanterosLibrary;

namespace AplicacionWeb
{
    public partial class Autentifica : System.Web.UI.Page
    {


        Usuario u;

        protected void Page_Load(object sender, EventArgs e)
        {
            UsuarioCAD cad = new UsuarioCAD();

            

            if (Session["Usuario"] == null)
            {
                Session.Add("Alert", "Has accedido a la página de autentificación sin estar registrado");
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                u = (Usuario)Session["Usuario"];
                if (cad.getAutentificado(u.Email)==1)
                {
                    Session.Add("Alert", "Has accedido a la página de autentificación con un usuario que ya está autentificado");
                    Response.Redirect("Inicio.aspx");
                }
                else
                {
                    labelEmail.Text = u.Email;
                }
            }
        }

        protected void botonAutentificar_Click(object sender, EventArgs e)
        {
            UsuarioCAD cad = new UsuarioCAD();
            int n = Convert.ToInt32(entryCodigo.Text);
            if(cad.getCodigo(u.Email)==n){
                if(!cad.autentificarUsu(u.Email)){
                    Session.Add("Alert","Ocurrió un error en la autentificación, vuelvelo a intentar más tarde");
                    Session.Remove("Usuario");
                    Response.Redirect("Inicio.aspx");
                }
                else{
                    Session.Add("Alert","Te has autentificado con éxito");
                    Response.Redirect("Inicio.aspx");
                }
            }
            else
            {
                Session.Add("Alert", "Código erroneo, vuelvelo a intentar haciendo login otra vez");
                Session.Remove("Usuario");
                Response.Redirect("Inicio.aspx");
            }
        }
    }
}