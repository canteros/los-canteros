﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LogoMaster.Master" AutoEventWireup="true" CodeBehind="Productos.aspx.cs" Inherits="AplicacionWeb.Productos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #divBotones {
            height: 600px;
            width: 17%;
            background-color:#3b6181;
            float:left;
            position:relative;
            left:2%;
            overflow-y:scroll;
            border:8px solid #5a88b0;
            border-radius:15px;
        }
        #divProducto {
            position:relative;
            float:right;
            height: 600px;
            width: 60%;
            right:20%;
       
            top: -1px;
        }
        #espacioFoto{
            width:40%;
            height:600px;
            float:left;
            position:relative;
            left:2%;
            margin-top:auto;
            margin-bottom:auto;
            
        }
        #datosProducto{
            float:right;
            position:relative;
            top: 10px;  
            bottom: 0;  
            background-color:#3b6181;
            right: -28%;  
            margin: auto;  
            width:28%;
            height:500px;
            border:3px solid #a9c8e4;
            color:white;
            font-weight:bold;
            text-align:left;
        }


        p.s{
            margin-left:5px;
            padding-bottom:15px;
        }
        .datosProductoLabel{
            color:#a9c8e4;
        }
        .botonesProductos {
            width: 130px;
            border: 1px solid white;
            padding: 5px;
            margin-left: 2px;
        }
        .esFav{
            position:absolute;
            left:90px;
            Color:yellow;
        }
        .imagenProducto{
            max-height:600px;
            max-width:800px;
            border:3px solid #a9c8e4;
            position: absolute;  
            top: 0;  
            bottom: 0;  
            left: 0;  
            right: 0;  
            margin: auto; 
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divBotones">
        <asp:Panel ID="botonesProductos" runat="server">



        </asp:Panel>

    </div>

    <div id="divProducto">
        <div id="espacioFoto">
            <asp:Image ID="imagenProducto" CssClass="imagenProducto" runat="server" />
        </div>

        <div id="datosProducto">
            <p class="s">Nombre: <asp:Label CssClass="datosProductoLabel" ID="labelNombre" runat="server" Text="Nombre"></asp:Label></p>
            <p class="s">Precio: <asp:Label CssClass="datosProductoLabel" ID="labelPrecio" runat="server" Text="Precio"></asp:Label></p>

            <asp:Panel ID="panelBotones" runat="server">

                <asp:Button ID="buttonFavorito" runat="server" style="float:right; position:relative; top:40px; right:120px;" Text="Es mi favorito" OnClick="buttonFavorito_Click" />
                <br><br><br>
                
                <br><br><br><br><br>
                <p style="padding:4px;"><i>Si haces un producto tu favorito cuando ya tenías otro, lo sustituirás.</i></p>


            </asp:Panel>
            <asp:Label ID="esFav" CssClass="esFav" runat="server" Text="<b><i>Es tu favorito</i></b>"></asp:Label>
        </div>
        <asp:Label style="visibility:collapse;" ID="labelOculto" runat="server" Text=""></asp:Label>
    </div>
</asp:Content>
