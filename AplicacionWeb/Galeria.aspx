﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true" Language="C#" MasterPageFile="~/LogoMaster.Master" AutoEventWireup="true" CodeBehind="Galeria.aspx.cs" Inherits="AplicacionWeb.Galeria" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #divBotones {
            height: 600px;
            width: 17%;
            background-color:#3b6181;
            float:left;
            position:relative;
            left:2%;
            overflow-y:scroll;
            border:8px solid #5a88b0;
            border-radius:15px;
        }
        #divFoto {
            position:relative;
            float:right;
            height: 600px;
            width: 60%;
            right:20%;
       
            top: -1px;
        }
        #espacioFoto{
            width:40%;
            height:600px;
            float:left;
            position:relative;
            left:2%;
            margin-top:auto;
            margin-bottom:auto;
            
        }
        .botonesGaleria{
            width:150px;
            border:1px solid white;
            padding:5px;
            margin-left:2px;
        }
        .imagenGaleria{
            max-height:600px;
            max-width:800px;
            border:3px solid #a9c8e4;
            position: absolute;  
            top: 0;  
            bottom: 0;  
            left: 0;  
            right: 0;  
            margin: auto;  
        }
        #datosFoto{
            float:right;
            position:relative;
            top: 10px;  
            bottom: 0;  
            background-color:#3b6181;
            right: -28%;  
            margin: auto;  
            width:28%;
            height:500px;
            border:3px solid #a9c8e4;
            color:white;
            font-weight:bold;
            text-align:left;
        }
        .buttonEliminar{
            color:#970000;
        }
        #eliminarDiv{
            position:relative;
            color:red;
            width:100px;
            height:1px;
            top:0px;
            left:5px;
            z-index:1;
        }
        p.s{
            margin-left:5px;
            padding-bottom:15px;
        }
        .datosFotoLabel{
            color:#a9c8e4;
        }
        .floatleft{
            float:left;
            margin-left:5px;
            z-index:50;
        }
        .fotostyle{
            max-height:150px;
            max-width:150px;
        }
        .dropdown{
            z-index:1000;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label style="position:fixed;top:0px;right:0px; display:none;" ID="labelOculto" runat="server" Text=""></asp:Label>
    <div id="divBotones"> 
        <asp:Panel ID="panelBotonesGaleria" runat="server">
        </asp:Panel>
    </div>
    <div id="divFoto">
        <div id="espacioFoto">
            <asp:Image ID="imagenSeleccionada" CssClass="imagenGaleria" runat="server" />
        </div>

        <div id="datosFoto">
            <p class="s">Nombre: <asp:Label CssClass="datosFotoLabel" ID="labelNombre" runat="server" Text="Nombre"></asp:Label></p>
            <p class="s">Uploader: <asp:Label CssClass="datosFotoLabel" ID="labelUploader" runat="server" Text="Uploader"></asp:Label></p>
            <p class="s">Fecha de subida: <asp:Label CssClass="datosFotoLabel" ID="labelFecha" runat="server" Text="Fecha"></asp:Label></p>
            <p class="s">Descripción: <asp:Label CssClass="datosFotoLabel" ID="labelDescripcion" runat="server" Text="Descripcion"></asp:Label></p>
            <p class="s">NúmeroVotos: <asp:Label CssClass="datosFotoLabel" ID="labelVotos" runat="server" Text="No hay votos"></asp:Label></p>
            <p class="s">Valoración media: <asp:Label CssClass="datosFotoLabel" ID="labelMedia" runat="server" Text="No hay votos"></asp:Label></p>
            <asp:Panel ID="panelValoracion" runat="server">
                <asp:Label ID="labelYaValorado" CssClass="floatleft" runat="server" Text="<i>Si ya has votado, se modificará tu voto</i>"></asp:Label>
            <p class="s" style="float:left;">Valorar: <asp:DropDownList CssClass="dropdown" ID="DropDownList1" runat="server" >
                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                        </asp:DropDownList></p>
                <asp:Button ID="buttonValorar" runat="server" OnClick="buttonValorar_Click" style="float:right; position:relative; top:13px; right:70px;" Text="Enviar Valoración" />
            </asp:Panel>
            <asp:Label ID="labelAviso" runat="server" Text="<i>Recuerda,  solo puedes valorar fotos si estás logeado</i>"></asp:Label>
            <div id="eliminarDiv">
            <asp:LinkButton ID="buttonEliminar" CssClass="buttonEliminar" runat="server" OnClick="buttonEliminar_Click"><b><u>Eliminar Foto</u></b></asp:LinkButton>
                </div>
            </div>
        
        </div>
</asp:Content>
