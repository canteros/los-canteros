﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CanterosLibrary;
using System.IO;

namespace AplicacionWeb
{
    public partial class SubirFoto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Usuario"] == null)
            {
                Session.Add("Alerta", "Has accedido a la página sin estar logeado");
            }
        }

        protected void ButtonSubirFoto_Click(object sender, EventArgs e)
        {
            FotoCAD cad = new FotoCAD();
            bool ok = false;
            string carpeta = Server.MapPath("~/usersgalery/");
            string tipoImagen = System.IO.Path.GetExtension(uploadFoto.FileName).ToLower();
            string[] tipos = { ".jpg", ".jpeg" };
            for (int i = 0; i < tipos.Length; i++)
            {
                if (tipos[i] == tipoImagen) ok = true;
            }

            if (!ok)
            {
                Session.Add("Alert", "Solo se admiten archivos jpg y jpeg");
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                Foto f = new Foto();
                f.Path = cad.generaId();
                f.Name = entryNombre.Text;
                f.Description = entryDescripcion.Text;
                f.UploadDate = DateTime.Now;
                Usuario uploader = (Usuario)Session["Usuario"];
                if (cad.altaFoto(f, uploader.Email))
                {
                    try
                    {
                        uploadFoto.SaveAs(carpeta+f.Path+".jpg");
                    }
                    catch (Exception ex)
                    {
                        Session.Add("Alert", "Error inesperado: " + ex.Message);
                        Response.Redirect("Inicio.aspx");
                    }
                    Session.Add("Alert", "Foto subida con éxito");
                    Response.Redirect("Inicio.aspx");
                }
                else
                {
                    Session.Add("Alert", uploader.Email );
                    Response.Redirect("Inicio.aspx");
                }
            }
        }
    }
}