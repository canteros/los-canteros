﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CanterosLibrary;
using System.Collections;

namespace AplicacionWeb
{
    public partial class adminMesas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Usuario"] == null)
            {
                Session.Add("Alert", "No tienes permiso para acceder a esta página");
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                Usuario u = (Usuario)Session["Usuario"];
                UsuarioCAD ucad = new UsuarioCAD();
                if (ucad.getTipo(u.Email) != "Admin")
                {
                    Session.Add("Alert", "No tienes permiso para acceder a esta página");
                    Response.Redirect("Inicio.aspx");
                }
            }


            MesaCAD cad = new MesaCAD();
            ArrayList mesas = cad.todoMesas();
            if (mesas.Count <= 0)
            {
                panelControles.Visible = false;
            }
            else
            {
                for (int i = 0; i < mesas.Count; i++)
                {
                    Mesa m = (Mesa)mesas[i];
                    TableRow row = new TableRow();
                    TableCell c1 = new TableCell();
                    TableCell c2 = new TableCell();
                    c1.Text = "Número: " + m.IdMesa.ToString();
                    c2.Text = "Capacidad: " + m.CapacidadMesa.ToString();
                    row.Cells.Add(c1);
                    row.Cells.Add(c2);
                    tablaMesas.Rows.Add(row);

                    ListItem item = new ListItem();
                    item.Text = m.IdMesa.ToString();
                    item.Value = m.IdMesa.ToString();
                    listEliminar.Items.Add(item);
                    listModificar.Items.Add(item);
                }
            }
            
        }

        protected void buttonAlta_Click(object sender, EventArgs e)
        {
            Mesa m = new Mesa();
            try
            {
                m.IdMesa = Convert.ToInt32(entryAltaNumero.Text);
                m.CapacidadMesa = Convert.ToInt32(entryAltaCapacidad.Text);
            }
            catch (Exception ex)
            {
                Session.Add("Alert", "Error, has introducido un dato no numérico");
                Response.Redirect("Inicio.aspx");
            }
            MesaCAD cad = new MesaCAD();
            if (cad.altaMesa(m))
            {
                Response.Redirect("adminMesas.aspx");
            }
            else
            {
                Session.Add("Alert", "Error, has introducido una mesa ya existente");
                Response.Redirect("Inicio.aspx");
            }
        }

        protected void buttonEliminar_Click(object sender, EventArgs e)
        {
            MesaCAD cad = new MesaCAD();
            int num = Convert.ToInt32(listEliminar.SelectedValue);
            if (cad.bajaMesa(num))
            {
                Response.Redirect("adminMesas.aspx");
            }
            else
            {
                Session.Add("Alert", "Error al eliminar mesa, vuelvelo a intentar por favor");
                Response.Redirect("Inicio.aspx");
            }
        }

        protected void buttonModificar_Click(object sender, EventArgs e)
        {
            MesaCAD cad = new MesaCAD();

            int num = Convert.ToInt32(listModificar.SelectedValue);
            int nuevo = Convert.ToInt32(entryModificarCapacidad.Text);
            Mesa m = new Mesa(2,nuevo);
            if (cad.modificarMesa(num, m))
            {
                Response.Redirect("adminMesas.aspx");
            }
            else
            {
                Session.Add("Alert", "Error al modificar mesa, vuelvelo a intentar por favor");
                Response.Redirect("Inicio.aspx");
            }
        }
    }
}