﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CanterosLibrary;
using AplicacionWeb;
using System.Net.Mail;

namespace AplicacionWeb
{



    public partial class LogoMaster : System.Web.UI.MasterPage
    {
        //Funcion que genera un popup con javascript, con "mensaje" como texto
        protected void PopUp(string mensaje)
        {
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message Box", "<script language='javascript'>alert('"+mensaje+"')</script>");
        }

        //Envia el email de autentificacion con el "codigo" insertado en el body del mensaje, a la "direccion"
        protected void EmailAutentificacion(int codigo,string direccion)
        {
            SmtpClient client = new SmtpClient("smtp.gmail.com",587);
            MailMessage msg= new MailMessage();
            try{
                MailAddress from = new MailAddress("loscanterosrockbar@gmail.com","Los Canteros RockBar");
                MailAddress to = new MailAddress(direccion,"Alias destinatario");
                msg.From = from;
                msg.To.Add(to);
                msg.Subject="Autentificación en Los Canteros RockBar";
                msg.IsBodyHtml = true;
                msg.Body="<p>Para finalizar tu registro en Los Canteros deberás introducir el siguiente código numérico:</p> <p><b>"+codigo+"</b></p><p>Un saludo, Los Canteros Team</p>";
                client.EnableSsl=true;
                client.Credentials = new System.Net.NetworkCredential("rockbarloscanteros@gmail.com","omargay12");
                client.Send(msg);
            }
            catch(Exception ex){
                PopUp("Error al enviar el email de confirmacion:"+ex.Message);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Usuario"] == null)
            {
                botonMostrarUsuarios.Visible = true;
                infoUser.Visible = false;
                menuAdmin.Visible = false;
            }
            else
            {
                botonMostrarUsuarios.Visible = false;
                UsuarioCAD cad = new UsuarioCAD();
                Usuario u = (Usuario)Session["Usuario"];
                if (cad.getAutentificado(u.Email) == 1)
                {
                    infoUser.Visible = true;
                    labelEmailUser.Text = u.Email;
                    labelNombreUser.Text = u.Nombre;
                    if (cad.getTipo(u.Email) == "Admin")
                    {
                        menuAdmin.Visible = true;
                    }
                    else
                    {
                        menuAdmin.Visible = false;
                    }
                }

            }
            if (this.ContentPlaceHolder1.Page is Autentifica)
            {
                infoUser.Visible = false;
                menuAdmin.Visible = false;
            }

            if (Session["Alert"] != null)
            {
                PopUp(Session["Alert"].ToString());
                Session.Remove("Alert");
            }
        }

        //Botones de navegación de interfaz
        protected void LogoInicio_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Inicio.aspx");
            
        }

        protected void LogoButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Productos.aspx");
        }

        protected void LogoButton2_Click(object sender, EventArgs e)
        {
            Response.Redirect("Reservas.aspx");
        }

        protected void LogoButton3_Click(object sender, EventArgs e)
        {
            Response.Redirect("Noticias.aspx");
        }

        protected void LogoButton4_Click(object sender, EventArgs e)
        {
            Response.Redirect("Nosotros.aspx");
        }

        protected void LogoButton5_Click(object sender, EventArgs e)
        {
            Response.Redirect("Galeria.aspx");
        }

        //Boton que se utiliza para hacer login
        protected void ButtonLoginContinuar_Click(object sender, EventArgs e)
        {
            UsuarioCAD cad = new UsuarioCAD();
            Usuario u = new Usuario(cad.getUsuario(entryLoginEmail.Text));
            if (u.Email == "")
            {
                PopUp("Usuario o contraseña erronea");
                
            }
            else
            {
                if (u.Contrasenya == entryLoginContrasenya.Text)
                {
                    if (cad.getAutentificado(u.Email) == 1)
                    {
                        Session.Add("Usuario", u);
                        botonMostrarUsuarios.Visible = false;
                        infoUser.Visible = true;
                        labelNombreUser.Text = u.Nombre;
                        labelEmailUser.Text = u.Email;
                        Response.Redirect("Inicio.aspx");
                    }
                    else
                    {
                        Session.Add("Usuario", u);
                        Response.Redirect("Autentifica.aspx");
                    }
                }
                else
                {
                    PopUp("Usuario o contraseña erronea");
                }
            }
        }

        //Boton que se utiliza para registrarse
        protected void ButtonRegistroContinuar_Click(object sender, EventArgs e)
        {
            UsuarioCAD cad = new UsuarioCAD();
            Usuario u = new Usuario();
            u.Email = entryRegistroEmail.Text;
            u.Contrasenya = entryRegistroContrasenya.Text;
            if (entryRegistroNombre.Text == "") u.Nombre = "Anónimo";
            else u.Nombre = entryRegistroNombre.Text;
            if (entryRegistroApellidos.Text == "") u.Apellidos = "Anonymous";
            else u.Apellidos = entryRegistroApellidos.Text;
            if (!cad.altaUsu(u)) PopUp("Error en la base de datos, no se pudo introducir usuario. Vuelvelo a intentar más tarde");
            else
            {
                int num;
                Random ran = new Random();
                num = ran.Next(1000, 9999);
                cad.setCodigo(u.Email, num);
                EmailAutentificacion(num, u.Email);
                Session.Add("Usuario", u);
                Response.Redirect("Autentifica.aspx");
            }

        }

        protected void linkSalir_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            botonMostrarUsuarios.Visible = true;
            infoUser.Visible = false;
            Response.Redirect("Inicio.aspx");
        }

        protected void linkSubirFoto_Click(object sender, EventArgs e)
        {
            Response.Redirect("SubirFoto.aspx");
        }

        protected void adminProductos_Click(object sender, EventArgs e)
        {
            Response.Redirect("adminProductos.aspx");
        }

        protected void adminMesas_Click(object sender, EventArgs e)
        {
            Response.Redirect("adminMesas.aspx");
        }

        protected void adminReservas_Click(object sender, EventArgs e)
        {
            Response.Redirect("adminReservas.aspx");
        }


    }
}