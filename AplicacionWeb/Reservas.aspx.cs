﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CanterosLibrary;
using System.Collections;

namespace AplicacionWeb
{
    public partial class Reservas : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Usuario"] == null)
            {
                Session.Add("Alert", "Error, si no estás logeado no puedes hacer reservas");
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                DateTime fecha = DateTime.Now.AddDays(-1);
                Usuario u = (Usuario)Session["Usuario"];
                ReservaCAD cad = new ReservaCAD();
                ArrayList reservasUsu = cad.buscarReservaUsuario(u.Email);
                for (int i = 0; i < reservasUsu.Count; i++)
                {
                    CanterosLibrary.Reservas r = (CanterosLibrary.Reservas)reservasUsu[i];
                    if (r.DiaReserva < fecha)
                    {
                        reservasUsu.RemoveAt(i);
                        i--;
                    }
                }

                if (reservasUsu.Count <= 0)
                {
                    labelNoReservas.Visible = true;
                    panelEliminar.Visible = false;
                }
                else
                {
                    labelNoReservas.Visible = false;
                    panelEliminar.Visible = true;
                    for (int i = 0; i < reservasUsu.Count; i++)
                    {
                        CanterosLibrary.Reservas r = (CanterosLibrary.Reservas)reservasUsu[i];
                        Label l = new Label();
                        string fot="No", cam="No";
                        if (r.FotorgafoReserva) fot = "Sí ";
                        if (r.CamareroPrivadoReserva) cam = "Sí";
                        l.ID = "label"+i;
                        l.CssClass = "claseLabelReserva";
                        l.Text = "Dia: " + r.DiaReserva.Day + "/" + r.DiaReserva.Month + "/" + r.DiaReserva.Year + "\t Mesa: " + r.Mesa + "\t Camarero Privado: " + cam + "\t Fotografo: "+fot;
                        panelReservas.Controls.Add(l);

                        ListItem item = new ListItem();
                        item.Text = r.DiaReserva.Day + "/" + r.DiaReserva.Month + "/" + r.DiaReserva.Year;
                        item.Value = r.DiaReserva.ToString();
                        listEliminar.Items.Add(item);

                    }
                }
            }
        }

        void b_Click(object sender, EventArgs e)
        {
            Response.Write("HOLA CRACK");
            Usuario u = (Usuario)Session["Usuario"];
            LinkButton b = (LinkButton)sender;
            ReservaCAD cad = new ReservaCAD();
            DateTime fecha = DateTime.Parse(b.ID);
            if (cad.bajaReserva(fecha, u.Email))
            {
                Session.Add("Alert", "Reserva eliminada con éxito");
                Response.Redirect("Inicio.apsx");
            }
            else
            {
                Session.Add("Alert", "Ocurrió un error al eliminar la reserva, vuelvelo a intentar por favor");
                Response.Redirect("Inicio.apsx");
            }
        }

        protected void calendarioReservas_SelectionChanged(object sender, EventArgs e)
        {
            DateTime fecha = calendarioReservas.SelectedDate;
            if (fecha < calendarioReservas.TodaysDate)
            {
                Session.Add("Alert", "Error, no puedes hacer una reserva en el pasado");
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                listMesas.Items.Clear();
                ReservaCAD fcad = new ReservaCAD();
                fcad.insertarDia(fecha);

                MesaCAD mcad = new MesaCAD();
                ArrayList mesas = mcad.todoMesas();

                ArrayList reservas = fcad.buscarReservaDia(fecha);

                for (int i = 0; i < reservas.Count; i++)
                {
                    CanterosLibrary.Reservas res = (CanterosLibrary.Reservas)reservas[i];
                    Mesa quitar = mcad.mesaId(res.Mesa);
                    for(int j=0;j<mesas.Count;j++){
                        Mesa comparar = (Mesa)mesas[j];
                        if (comparar.IdMesa == quitar.IdMesa)
                        {
                            mesas.RemoveAt(i);
                            j = mesas.Count + 1;
                            //i--;
                        }
                    }
                }


                for (int i = 0; i < mesas.Count; i++)
                {
                    ListItem item = new ListItem();
                    Mesa m = (Mesa)mesas[i];
                    item.Text = m.CapacidadMesa.ToString();
                    item.Value = m.IdMesa.ToString();
                    listMesas.Items.Add(item);
                }
            }
        }

        protected void buttonReserva_Click(object sender, EventArgs e)
        {
            CanterosLibrary.Reservas res = new CanterosLibrary.Reservas();
            Usuario u = (Usuario)Session["Usuario"];
            ListItem fotografo = checkFotografoCamarero.Items[0];
            ListItem camarero = checkFotografoCamarero.Items[1];
            string aux = listMesas.SelectedValue;
            res.Mesa = Convert.ToInt32(aux);
            res.Usuario = u.Email;
            res.DiaReserva = calendarioReservas.SelectedDate;
            if (fotografo.Selected) res.FotorgafoReserva = true;
            if (camarero.Selected) res.CamareroPrivadoReserva = true;

            ReservaCAD cad = new ReservaCAD();
            if (cad.altaReserva(res))
            {
                Session.Add("Alert", "Reserva realizada con éxito");
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                Session.Add("Alert", "No puedes hacer dos reservas el mismo día, eso sería acaparar D:");
                Response.Redirect("Inicio.aspx");
            }
        }

        protected void linkEliminar_Click(object sender, EventArgs e)
        {
            Usuario u = (Usuario)Session["Usuario"];
            ReservaCAD cad = new ReservaCAD();
            DateTime fecha = DateTime.Parse(listEliminar.SelectedValue);
            if (cad.bajaReserva(fecha, u.Email))
            {
                Session.Add("Alert", "Reserva eliminada con éxito");
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                Session.Add("Alert", "Ocurrió un error al eliminar la reserva, vuelvelo a intentar por favor");
                Response.Redirect("Inicio.aspx");
            }
        }
    }
}