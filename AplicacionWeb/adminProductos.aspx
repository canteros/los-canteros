﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LogoMaster.Master" AutoEventWireup="true" CodeBehind="adminProductos.aspx.cs" Inherits="AplicacionWeb.adminProductos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #divInsertarProducto{
            position:absolute;
            left:0px;
            right:0px;
            margin-left:auto;
            margin-right:auto;
            width:900px;
            height:70px;
            top:30px;
        }
        #requeridoNombre{
            position:absolute;
            bottom:20px;
        }
        #requeridoPrecio{
            bottom:0px;
        }
        #listaProductos{
            position:absolute;
            left:0px;
            right:0px;
            margin-left:auto;
            margin-right:auto;
            width:500px;
            height:400px;
            top:150px;
            right:300px;
            text-align:center;
            overflow-y:auto;
        }
        #otrosControles{
            position:absolute;
            left:0px;
            right:0px;
            margin-left:auto;
            margin-right:auto;
            width:500px;
            height:400px;
            top:150px;
            left:300px;
            text-align:center;
        }
        .styleAlta{
            float:left;
            padding-right:15px;
            padding-left:15px;
        }
        .tablaP{
            text-align:left;
            padding:2px;
            border:2px solid black;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div id="divInsertarProducto">
        <b style="text-align:center; position:relative; left:400px;">Alta de Productos:</b>
    <p></p>
        <span class="styleAlta">Nombre: </span><asp:TextBox CssClass="styleAlta" style="width:90px;" ID="entryAltaNombre" runat="server"></asp:TextBox>
        <span class="styleAlta">Precio: </span><asp:TextBox CssClass="styleAlta" style="width:25px;" ID="entryAltaPrecio" runat="server"></asp:TextBox>
        <span class="styleAlta">Foto: </span><asp:FileUpload CssClass="styleAlta" ID="altaFoto" runat="server" />
        <asp:Button CssClass="styleAlta" ID="buttonAlta" runat="server" Text="Dar de Alta" OnClick="buttonAlta_Click" ValidationGroup="altaValidation" />
        <br><br>
        <asp:RequiredFieldValidator ID="requeridoNombre" CssClass="errorValidation" runat="server" ErrorMessage="Debes de poner un nombre" ValidationGroup="altaValidation" ControlToValidate="entryAltaNombre"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="requeridoPrecio" CssClass="errorValidation" runat="server" ErrorMessage="Debes de poner un precio" ControlToValidate="entryAltaPrecio" ValidationGroup="altaValidation"></asp:RequiredFieldValidator>
    </div>
    <div id="listaProductos">
        <b style="position:absolute; left:40px;">Lista de productos: </b><br><br>
        <asp:Table ID="tablaProductos" runat="server"></asp:Table>
    </div>

    <div id="otrosControles">
        <asp:Panel ID="panelControles" runat="server">
        
        
        <b>Eliminar Producto:</b>
            ID: <asp:DropDownList ID="listEliminar" runat="server"></asp:DropDownList>
            <asp:Button ID="buttonEliminar" runat="server" Text="Eliminar Producto" OnClick="buttonEliminar_Click" />


            <br><br><br>
            <b>Modificar Producto:</b>
        <br><br>
        ID: <asp:DropDownList ID="listaModificar" runat="server">
            </asp:DropDownList>
            
            

        Nombre: <asp:TextBox ID="entryModificarNombre" runat="server"></asp:TextBox>
        Precio: <asp:TextBox ID="entryModificarPrecio" style="width:30px;" runat="server"></asp:TextBox>
        <br><br>

        <asp:Button ID="buttonModificar" runat="server" Text="ModificarProducto" OnClick="buttonModificar_Click" />
        </asp:Panel>
    </div>

</asp:Content>
