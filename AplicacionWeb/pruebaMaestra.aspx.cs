﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CanterosLibrary;
using System.Collections;

namespace AplicacionWeb
{
    public partial class pruebaMaestra : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Usuario u = new Usuario("gmail.com","nombre","apellidos","cacaman");
            Session.Add("Usuario", u);
            Control c = Master.FindControl("botonMostrarUsuarios");
            c.Visible = false;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Session.Remove("Usuario");
            Control c = Master.FindControl("botonMostrarUsuarios");
            c.Visible = true;
        }
    }
}