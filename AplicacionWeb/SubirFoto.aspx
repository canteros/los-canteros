﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LogoMaster.Master" AutoEventWireup="true" CodeBehind="SubirFoto.aspx.cs" Inherits="AplicacionWeb.SubirFoto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="espacioSubirFoto" style="text-align:center; color:white; position:relative; right:30px;">
        <p><span style="text-align:left;">Nombre: </span><asp:TextBox Width="150" ID="entryNombre" runat="server" AutoCompleteType="Disabled"></asp:TextBox></p>
        <p style="position:relative; right:12px;">Descripción: <asp:TextBox Width="150" ID="entryDescripcion" runat="server" TextMode="MultiLine" AutoCompleteType="Disabled"></asp:TextBox></p>
        <p><asp:FileUpload ID="uploadFoto" runat="server" /></p>
        <p><asp:Button CssClass="botonSubir" ID="ButtonSubirFoto" runat="server" Text="" ValidationGroup="fotoGroup" OnClick="ButtonSubirFoto_Click" /></p>
        <asp:RequiredFieldValidator ID="checkFoto" runat="server" ErrorMessage="No has introducido ninguna foto" ControlToValidate="uploadFoto" ValidationGroup="fotoGroup"></asp:RequiredFieldValidator>
        </div>
</asp:Content>
