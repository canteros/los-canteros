using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
 
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;

namespace CanterosLibrary{

    public class UsuarioCAD
    {

        private string conexion;

        public UsuarioCAD()
        {
            conexion = Constantes.dbConexion;

        }

        //Funcion que devuelve un usuario dado un email, si este no est� devuelve un usuario vac�o
        public Usuario getUsuario(string email)
        {


            Usuario u = new Usuario();
            SqlConnection conec = new SqlConnection(conexion);
            conec.Open();
            SqlCommand com = new SqlCommand("Select * from Usuarios where email='" + email+"'", conec);
            SqlDataReader dr = com.ExecuteReader();

            if(dr.Read())
            {
                string nAux = dr["Nombre"].ToString();
                u.Nombre = nAux;

                string pAux = dr["Apellidos"].ToString();
                u.Apellidos = pAux;
               
                string cAux = dr["Pass"].ToString();
                u.Contrasenya = cAux;

                string eAux = dr["Email"].ToString();
                u.Email = eAux;

            }


            conec.Close();
            return u;
        }

        //Funcion que, pasandole un usuario, lo inserta en la BD
        //Si todo es correcto devuelve true, si hay error en la inserci�n devuelve false
        public bool altaUsu(Usuario p)
        {
            bool au = false;
            //rellena usuario
            try
            {
                SqlConnection conec = new SqlConnection(conexion);
                conec.Open();
                SqlCommand com = new SqlCommand("Insert Into Usuarios (Email,Nombre,Apellidos,Pass) VALUES ('"+p.Email+"', '" + p.Nombre + "', '" + p.Apellidos + "', '"+p.Contrasenya +"')", conec);
                if (com.ExecuteNonQuery() == 1) au = true;
                conec.Close();
            }
            catch (Exception ex) { return false; }
             
            
            

            return au;
        }

        //Funcion que, dado un email, da de baja un usuario
        //Devuelve true si lo elimina correctamente, false en caso contrario
        public bool bajaUsu(string email)
        {
            bool bu = true;
            try
            {
                SqlConnection conec = new SqlConnection(conexion);
                conec.Open();
                SqlCommand com = new SqlCommand("Delete from Usuarios where Email='" + email + "'", conec);
                if (com.ExecuteNonQuery() == 1) bu = true;
                conec.Close();
            }
            catch (Exception ex) { return false; }

            return bu;

        }

        //Funcion que coge los datos del usuario "u" y los inserta en el usuario con id "email"(todos los datos se cambian salvo la id)
        //Devuelve true si  se ha realizado correctamente, else en caso contrario
        public bool modificaUsu(string email, Usuario u)
        {

            bool mu = true;
            try
            {
                SqlConnection conec = new SqlConnection(conexion);
                conec.Open();
                SqlCommand com = new SqlCommand("Update Usuarios set Nombre='"+u.Nombre+"', Apellidos='"+u.Apellidos+"', Pass='"+u.Contrasenya+
                    "' where Email='"+email+"'", conec);
                if (com.ExecuteNonQuery() == 1) mu = true;
                conec.Close();
            }
            catch (Exception ex) { return false; }
            return mu;
        }


        

        //Funcion que devuelve todos los usuarios por busqueda de apellidos
        public ArrayList usuarioApellidos(string apellidos)
        {
            ArrayList datos = new ArrayList();
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand com = new SqlCommand("select * from Usuarios where apellidos='" + apellidos + "'", con);
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
            {
                Usuario u = new Usuario();
                u.Email = dr["Email"].ToString();
                u.Nombre = dr["Nombre"].ToString();
                u.Apellidos = dr["Apellidos"].ToString();
                u.Contrasenya = dr["Pass"].ToString();
                datos.Add(u);
            }

            return datos;
        }

        //Funcion que devuelve todos los usuarios por busqueda de nombre
        public ArrayList usuarioNombre(string nombre)
        {
            ArrayList datos = new ArrayList();
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand com = new SqlCommand("select * from Usuarios where nombre='" + nombre + "'", con);
            SqlDataReader dr = com.ExecuteReader();
            while (dr.Read())
            {
                Usuario u = new Usuario();
                u.Email = dr["Email"].ToString();
                u.Nombre = dr["Nombre"].ToString();
                u.Apellidos = dr["Apellidos"].ToString();
                u.Contrasenya = dr["Pass"].ToString();
                datos.Add(u);
            }

            return datos;
        }

        //Devuelve todos los usuarios
        public ArrayList usuarioTodos()
        {
            {
                ArrayList datos = new ArrayList();
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand com = new SqlCommand("select * from Usuarios", con);
                SqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    Usuario u = new Usuario();
                    u.Email = dr["Email"].ToString();
                    u.Nombre = dr["Nombre"].ToString();
                    u.Apellidos = dr["Apellidos"].ToString();
                    u.Contrasenya = dr["Pass"].ToString();
                    datos.Add(u);
                }

                return datos;
            }
        }
        //Autentifica el usuario (-1 es sin autentificar, 1 es autentificado)
        //Devuelve true si autentificado con exito, false en caso contrario
        public bool autentificarUsu(string email)
        {
            bool au = true;
            try
            {
                SqlConnection conec = new SqlConnection(conexion);
                conec.Open();
                SqlCommand com = new SqlCommand("Update Usuarios set Autentificado="+1+" where Email='"+email+"'",conec);
                if (com.ExecuteNonQuery() == 1) au = true;
                conec.Close();
            }
            catch (Exception ex) { return false; }
            return au;
        }

        //Comprueba que el c�digo "codigo" es igual que el del usuario "email"
        //true en caso de que sean el mismo, false en caso contrario
        public int getCodigo(string email)
        {
            int n = -1;

            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand com = new SqlCommand("Select * from Usuarios where Email='"+email+"'", con);
            SqlDataReader dr = com.ExecuteReader();
            if (dr.Read())
            {
                n = Convert.ToInt32(dr["CodigoAunt"].ToString());
            }
            con.Close();
            return n;
        }

        //Modifica el codigo del usuario "email" por el "codigo"
        //true si lo hace correctamente, false en caso contrario
        public bool setCodigo(string email, int codigo)
        {
            bool au = true;
            try
            {
                SqlConnection conec = new SqlConnection(conexion);
                conec.Open();
                SqlCommand com = new SqlCommand("Update Usuarios set CodigoAunt=" + codigo + " where Email='" + email + "'", conec);
                if (com.ExecuteNonQuery() == 1) au = true;
                conec.Close();
            }
            catch (Exception ex) { return false; }
            return au;
        }
        
        //Modifica el tipo de usuario "email" (0 para usuarios normales, 1 para admins)
        //true si lo hace correctamente, false en caso contrario
        public bool setTipo(string email, int tipo){
            bool au = true;
            try
            {
                SqlConnection conec = new SqlConnection(conexion);
                conec.Open();
                SqlCommand com = new SqlCommand("Update Usuarios set TipoUser=" + tipo + " where Email='" + email + "'", conec);
                if (com.ExecuteNonQuery() == 1) au = true;
                conec.Close();
            }
            catch (Exception ex) { return false; }
            return au;
        }

        //Devuelve el tipo de usuario que es "email"
        //Devuelve cadena vac�a en caso de no encontrar ningun usuario
        public string getTipo(string email)
        {
            String s = "";

            SqlConnection conec = new SqlConnection(conexion);
            conec.Open();
            SqlCommand com = new SqlCommand("Select * from Usuarios where email='" + email + "'", conec);
            SqlDataReader dr = com.ExecuteReader();

            if (dr.Read())
            {
                int x = Convert.ToInt32(dr["TipoUser"].ToString());
                if (x == 0) s = "Normal";
                else if (x == 1) s = "Admin";
            }

            return s;
        }

        //Devuelve si un usuario est� autentificado
        public int getAutentificado(string email)
        {
            int n = -10;

            SqlConnection conec = new SqlConnection(conexion);
            conec.Open();
            SqlCommand com = new SqlCommand("Select * from Usuarios where email='" + email + "'", conec);
            SqlDataReader dr = com.ExecuteReader();

            if (dr.Read())
            {
                n = Convert.ToInt32(dr["Autentificado"].ToString());
            }
            return n;
        }

    }

}