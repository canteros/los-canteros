﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

using System.Collections;
using System.Configuration;
using System.Data.SqlClient;



namespace CanterosLibrary
{
    public class FotoCAD
    {
        private string conexion;

        public FotoCAD()
        {
            //get conexion
            conexion = Constantes.dbConexion;
        }


    

        //Funcion para conseguir la info de una foto con su Path, si no la encuentra retorna una foto
        //vacia
        public Foto dameFoto(string id)
        {
            Foto f = new Foto();
            //select para conseguir la foto utilizando como referencia su path (cP)

            SqlConnection con = new SqlConnection(conexion);
            con.Open();

            SqlCommand com = new SqlCommand("Select * from Fotos where Path='" + id + "'", con);
            SqlDataReader dr = com.ExecuteReader();

            if (dr.Read())
            {

                string nAux = dr["Path"].ToString();
                f.Path = nAux;

                string dAux = dr["Nombre"].ToString();
                f.Name = dAux;

                string uAux = dr["Descripcion"].ToString();
                f.Description = uAux;

                DateTime fAux = DateTime.Parse( dr["FechaSubida"].ToString() );
                f.UploadDate = fAux;
            }

            con.Close();
            return f;
        }

            //Funcion que da de alta una foto con un usuario

            public bool altaFoto(Foto f, string usuario)
        {
            bool added = false;
            try
            {
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand com = new SqlCommand("Insert Into Fotos (Path, Nombre, Descripcion, FechaSubida, Usuario) VALUES ('"+ f.Path +"', '"+f.Name+ "', '"+f.Description + "', '"+f.UploadDate.Year + "/"+f.UploadDate.Month+"/"+f.UploadDate.Day+"', '"+ usuario+  "') ", con);
                if (com.ExecuteNonQuery() == 1) added = true;
                con.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return added;
        }





        

        //Funcion que elimina una foto tomando como referencia el Path de la foto
        public bool bajaFoto(string id)
        {
            bool deleted = false;
            try
            {
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand com = new SqlCommand("Delete from Fotos where Path='" + id + "'", con);

                if (com.ExecuteNonQuery() == 1) deleted = true;

                con.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return deleted;

        }

        public bool modificaFoto(string id, Foto f)
        {
            //modifica la foto con esa id y le pone los atributos de la Foto f 
            bool update = false;
            try
            {
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand com = new SqlCommand("UPDATE Fotos SET Nombre='" + f.Name +"', Descripcion='"+f.Description+ "',FechaSubida='"+f.UploadDate+"' where Path='" +id + "'" ,con);
                if (com.ExecuteNonQuery() == 1) update = true;
                con.Close();
            
            
            }
            catch (Exception ex)
            {
                return false;
            }
            return update;
        }
        
        //funcion que retorna el uploader de una foto con su email
        public string uploaderFoto(string id)
        {
            string uploader = "";


            SqlConnection con = new SqlConnection(conexion);
            con.Open();

            SqlCommand com = new SqlCommand("Select Usuario from Fotos where Path='" + id + "'", con);
            SqlDataReader dr = com.ExecuteReader();
            if (dr.Read()) uploader = dr["Usuario"].ToString();

            con.Close();
            
            return uploader;
        }
        //Funcion que devuelve las veces que una foto ha sido votada
        public int numeroVotos(string id)
        {
            int n = -1;

            SqlConnection con = new SqlConnection(conexion);
            con.Open();

            SqlCommand com = new SqlCommand("Select count(*) valoracion  from valorar_foto where foto='" + id + "'", con);
            SqlDataReader dr = com.ExecuteReader();
             if (dr.Read()) n =  Convert.ToInt32(dr["valoracion"].ToString());
            con.Close();
            return n;
        }
        //Funcion que devuelve la puntuacion total de una foto
        public float totalVotos(string id)
        {
            float n = -1.00F;


            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand com = new SqlCommand("Select SUM(valoracion) from valorar_foto where foto='" + id + "'", con);
            SqlDataReader dr = com.ExecuteReader();
            if (dr.Read()) n = Convert.ToSingle(dr[0].ToString());
            con.Close();
            return n;
            
        }

        public float mediaVotos(string id)
        {
                    return totalVotos(id)/numeroVotos(id) ;
        }

        //Devuelve todas las fotos dado un email, si no lo encuentra retorna un array vacio
        public ArrayList dameFotosUsuario(string id)
        {
            ArrayList datos = new ArrayList();      
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand com = new SqlCommand("Select * from Fotos where Usuario='" + id  + "'", con);
            SqlDataReader dr = com.ExecuteReader();

             while (dr.Read())
             {
                 Foto m = new Foto();

                 string idAux = dr["Path"].ToString();
                 m.Path = idAux;
                 string cAux = dr["Nombre"].ToString();
                 m.Name = cAux;
                 string dAux = dr["Descripcion"].ToString();
                 m.Description = dAux;
                 DateTime fechaAux = (DateTime) dr["FechaSubida"];
                 m.UploadDate = fechaAux;
                 datos.Add(m);
             }

             con.Close();
            return datos;
        }
 

        public ArrayList fotosNombre(string nombre)
        {

            ArrayList datos = new ArrayList();
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand com = new SqlCommand("Select * from Fotos where Nombre='" + nombre + "'", con);
            SqlDataReader dr = com.ExecuteReader();

            while (dr.Read())
            {
                Foto m = new Foto();

                string idAux = dr["Path"].ToString();
                m.Path = idAux;
                string cAux = dr["Nombre"].ToString();
                m.Name = cAux;
                string dAux = dr["Descripcion"].ToString();
                m.Description = dAux;
                DateTime fechaAux = (DateTime)dr["FechaSubida"];
                m.UploadDate = fechaAux;
                datos.Add(m);
            }

            con.Close();
            return datos;
            
        }

        public string generaId()
        {
            string id = "";
            ArrayList datos = new ArrayList();
            int n = 0;
            int max = 0;
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand com = new SqlCommand("select substring(Path,3,50) from Fotos;", con);
            SqlDataReader dr = com.ExecuteReader();

            while (dr.Read())
            {

                n = Convert.ToInt32(dr[0]);

                if (n > max)
                    max = n;
            }
            max = max + 1;
            id = "id" + max;
            return id;
        }

        public ArrayList todoFotos(){
        ArrayList datos = new ArrayList();
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand com = new SqlCommand("Select * from Fotos", con);
            SqlDataReader dr = com.ExecuteReader();

            while (dr.Read())
            {
                Foto m = new Foto();

                string idAux = dr["Path"].ToString();
                m.Path = idAux;
                string cAux = dr["Nombre"].ToString();
                m.Name = cAux;
                string dAux = dr["Descripcion"].ToString();
                m.Description = dAux;
                DateTime fechaAux = (DateTime)dr["FechaSubida"];
                m.UploadDate = fechaAux;
                datos.Add(m);
            }

            con.Close();
            return datos;
            
        }

        public bool altaValoracion(string usuario, string foto, int valoracion)
        {
            {
                bool added = false;
                try
                {
                    SqlConnection con = new SqlConnection(conexion);
                    con.Open();
                    SqlCommand com = new SqlCommand("Insert into valorar_foto (usuario,foto,valoracion) values('"+usuario+"', '"+foto+"', "+valoracion+")", con);
                    if (com.ExecuteNonQuery() == 1) added = true;
                    con.Close();
                }
                catch (Exception ex)
                {
                    return false;
                }
                return added;
            }
        }

        public bool bajaValoracion(string usuario, string foto)
        {
            {
                bool deleted = false;
                try
                {
                    SqlConnection con = new SqlConnection(conexion);
                    con.Open();
                    SqlCommand com = new SqlCommand("delete from valorar_foto where usuario='"+usuario+"' and foto='" + foto +"'", con);
                    if (com.ExecuteNonQuery() == 1) deleted = true;
                    con.Close();
                }
                catch (Exception ex)
                {
                    return false;
                }
                return deleted;
            }
        }
    }
}
