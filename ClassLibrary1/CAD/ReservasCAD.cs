﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using System.Data.SqlClient;
using System.Collections;

namespace CanterosLibrary
{
    public class ReservaCAD
    {
            private string conexion;

            public ReservaCAD()
            {
                conexion = Constantes.dbConexion;
            }
            
            //Busca una reserva con la clave primaria
            //Devuelve la reserva, o una reserva vacía en caso de no encontrar ninguna
            public Reservas buscoReservas(DateTime dia, int mesa, string email)
            {
                Reservas r = new Reservas();
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand com = new SqlCommand("select * from Reserva where Horario='"+dia+"'AND Usuario='"+email+"'AND Mesa="+mesa,con);
                SqlDataReader dr = com.ExecuteReader();
                if (dr.Read())
                {
                    r.DiaReserva = DateTime.Parse(dr["Horario"].ToString());
                    r.Mesa = Convert.ToInt32(dr["Mesa"].ToString());
                    r.Usuario = dr["Usuario"].ToString();
                    if(dr["CamareroPrivado"].ToString()=="1") r.CamareroPrivadoReserva=true;
                    else r.CamareroPrivadoReserva=false;
                    if(dr["Fotografo"].ToString()=="1") r.FotorgafoReserva=true;
                    else r.FotorgafoReserva=false;
                }
                con.Close();

                return r;
            }
            
            //Da de alta una reserva "r"
            //Retorna true si lo ha conseguido, false en caso contrario
            public bool altaReserva(Reservas r)
            {
                bool au = false;
                
                try
                {
                    SqlConnection conec = new SqlConnection(conexion);
                    conec.Open();
                    int camAux = -1, fotAux = -1;
                    if (r.FotorgafoReserva) fotAux = 1;
                    if (r.CamareroPrivadoReserva) camAux = 1;
                    SqlCommand com = new SqlCommand("Insert Into Reserva (Usuario,Horario,Mesa,CamareroPrivado,Fotografo) VALUES ('" + r.Usuario + "', '" + r.DiaReserva.Year + "/" + r.DiaReserva.Month + "/" + r.DiaReserva.Day + "', " + r.Mesa + ", " + camAux + ", " + fotAux + ")", conec);
                    if (com.ExecuteNonQuery() == 1) au = true;
                    conec.Close();
                }
                catch (Exception ex) { return false; }




                return au;
            }
           
           //Da de baja una reserva con su clave primaria
           //Retorna true si la ha dado de baja, else en caso contrario
           public bool bajaReserva(DateTime dia, string usuario)
            {
                bool au = false;

                try
                {
                    SqlConnection conec = new SqlConnection(conexion);
                    conec.Open();
                    SqlCommand com = new SqlCommand("Delete from Reserva where Horario='" + dia.Year + "/" + dia.Month + "/" + dia.Day + "' AND Usuario='" + usuario + "'", conec);
                    if (com.ExecuteNonQuery() == 1) au = true;
                    conec.Close();
                }
                catch (Exception ex) { return false; }




                return au;
            }
           
           //Modifica la reserva (solo los datos de camarero y fotografo)
           public bool modificaReserva(DateTime dia, string usuario, bool camarero, bool fotografo)
           {
               bool au = false;

               int camAux = -1, fotAux = -1;
               if (fotografo) fotAux = 1;
               if (camarero) camAux = 1;

               try
               {
                   SqlConnection conec = new SqlConnection(conexion);
                   conec.Open();
                   SqlCommand com = new SqlCommand("Update Reservas set CamareroPrivado=" + camAux + ", Fotografo=" + fotAux + " where Usuario='" + usuario + "' AND Horario='" + dia.Year + "/" + dia.Month + "/" + dia.Day + "'", conec);
                   if (com.ExecuteNonQuery() == 1) au = true;
                   conec.Close();
               }
               catch (Exception ex) { return false; }




               return au;
           }

            //Devuelve todas las instancias de Reservas en las que aparece la mesa "mesa"
            public ArrayList buscarReservaMesa(int mesa)
            {
                ArrayList datos = new ArrayList();
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand com = new SqlCommand("Select * from Reserva where mesa=" + mesa+" order by ASC", con);
                SqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    Reservas r = new Reservas();
                    if (Convert.ToInt32(dr["CamareroPrivado"].ToString()) == 1) r.CamareroPrivadoReserva = true;
                    if (Convert.ToInt32(dr["Fotografo"].ToString()) == 1) r.FotorgafoReserva = true;
                    r.Usuario = dr["Usuario"].ToString();
                    r.DiaReserva = DateTime.Parse(dr["Horario"].ToString());
                    r.Mesa = Convert.ToInt32(dr["Mesa"].ToString());

                    datos.Add(r);
                }
                return datos;
            }

            //Devuelve todas las Reservas del mismo dia
            public ArrayList buscarReservaDia(DateTime dia)
            {
                ArrayList datos = new ArrayList();
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand com = new SqlCommand("Select * from Reserva where Horario='" + dia.Year + "/" + dia.Month + "/" + dia.Day + "' order by Horario ASC", con);
                SqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    Reservas r = new Reservas();
                    if (Convert.ToInt32(dr["CamareroPrivado"].ToString()) == 1) r.CamareroPrivadoReserva = true;
                    if (Convert.ToInt32(dr["Fotografo"].ToString()) == 1) r.FotorgafoReserva = true;
                    r.Usuario = dr["Usuario"].ToString();
                    r.DiaReserva = DateTime.Parse(dr["Horario"].ToString());
                    r.Mesa = Convert.ToInt32(dr["Mesa"].ToString());

                    datos.Add(r);
                }
                return datos;
            }

            //Devuelve todas las reservas hechas por un usuario
            public ArrayList buscarReservaUsuario(string usuario)
            {
                ArrayList datos = new ArrayList();
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand com = new SqlCommand("Select * from Reserva where Usuario='" + usuario + "' order by Horario ASC", con);
                SqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    Reservas r = new Reservas();
                    if (Convert.ToInt32(dr["CamareroPrivado"].ToString()) == 1) r.CamareroPrivadoReserva = true;
                    if (Convert.ToInt32(dr["Fotografo"].ToString()) == 1) r.FotorgafoReserva = true;
                    r.Usuario = dr["Usuario"].ToString();
                    r.DiaReserva = DateTime.Parse(dr["Horario"].ToString());
                    r.Mesa = Convert.ToInt32(dr["Mesa"].ToString());

                    datos.Add(r);
                }
                return datos;
            }

        //Inserta en la tabla HorarioReservas un día
            public bool insertarDia(DateTime dia)
            {
                {
                    bool au = false;
                    try
                    {
                        SqlConnection conec = new SqlConnection(conexion);
                        conec.Open();
                        SqlCommand com = new SqlCommand("Insert into HorarioReservas (Dia) VALUES('"+dia.Year+"/"+dia.Month+"/"+dia.Day+"')", conec);
                        if (com.ExecuteNonQuery() == 1) au = true;
                        conec.Close();
                    }
                    catch (Exception ex) { return false; }

                    return au;
                }
            }

            public ArrayList todoReservas()
            {
                ArrayList datos = new ArrayList();
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand com = new SqlCommand("Select * from Reserva order by Horario ASC", con);
                SqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    Reservas r = new Reservas();
                    if (Convert.ToInt32(dr["CamareroPrivado"].ToString()) == 1) r.CamareroPrivadoReserva = true;
                    if (Convert.ToInt32(dr["Fotografo"].ToString()) == 1) r.FotorgafoReserva = true;
                    r.Usuario = dr["Usuario"].ToString();
                    r.DiaReserva = DateTime.Parse(dr["Horario"].ToString());
                    r.Mesa = Convert.ToInt32(dr["Mesa"].ToString());

                    datos.Add(r);
                }
                return datos;
            }
    }
}
