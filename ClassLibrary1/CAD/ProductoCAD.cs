﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
namespace CanterosLibrary

{
    public class ProductoCAD
    {
            private string conexion;

            public ProductoCAD()
            {
                //consigue la conexion
                conexion = Constantes.dbConexion;

            }
            
            //funcion que devuelve todos los datos del producto con su id
            public Producto dameProducto(string id)
            {
                Producto p = new Producto();
                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                SqlCommand com = new SqlCommand("Select * from Producto where ID='" + id + "'", con);
                SqlDataReader dr = com.ExecuteReader();

                if (dr.Read())
                {
                    p.IdProducto = dr["ID"].ToString();
                    p.NombreProducto = dr["Nombre"].ToString();
                    p.PrecioProducto = Convert.ToSingle(dr["Precio"].ToString());

                }

                con.Close();
              
                return p;
            }

        //Funcion que sube un producto tomando como referncia un objeto y su imagen
        public bool altaProducto(Producto p,string foto)
            {
                bool ap = true;
                try
                {
                    SqlConnection con = new SqlConnection(conexion);
                    con.Open();

                    string s = p.PrecioProducto.ToString();
                           s = s.Replace(',', '.');
                    SqlCommand com = new SqlCommand("Insert Into Producto (ID, Nombre, Precio, Foto) VALUES ('" + p.IdProducto + "', '" + p.NombreProducto + "', " + s + ", '" + foto + "') ", con);
                    if (com.ExecuteNonQuery() == 1) ap = true;
                    con.Close();
                }
                catch (Exception ex)
                {
                    return false;
                }           
                return ap;
            }

        //Borra un producto de la base de datos con referencia de la id
            public bool bajaProducto(string id)
            {
                bool ap = false;                
                try
                {
                    SqlConnection con = new SqlConnection(conexion);
                    con.Open();
                    SqlCommand com = new SqlCommand("Delete from Producto where ID='" + id + "'", con);

                    if (com.ExecuteNonQuery() == 1) ap = true;

                    con.Close();
                }
                catch (Exception ex)
                {
                    return false;
                }

                return ap;
            }
        //Cambia los atributos de un producto con ID id a los del producto p
            public bool modificaProducto(string id, Producto p)
            {


                bool update = false;
                try
                {
                    SqlConnection con = new SqlConnection(conexion);
                    con.Open();

                    string s = p.PrecioProducto.ToString();
                    s = s.Replace(',', '.');

                    SqlCommand com = new SqlCommand("UPDATE Producto SET Nombre='" + p.NombreProducto + "', Precio="+ s +" where ID='" +id + "'" ,con);
                    if (com.ExecuteNonQuery() == 1) update = true;
                    con.Close();
                }
                    catch (Exception ex)
                {
                     return false;
                }
                return update;
            }


                //busca el producto en el cual esté el nombre (puede haber varios productos con el mismo nombre)

            public ArrayList productoNombre(string nombre)
            {
                ArrayList datos = new ArrayList();
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand com = new SqlCommand("Select * from Producto where Nombre like '%" + nombre + "%'", con);
                SqlDataReader dr = com.ExecuteReader();

                while (dr.Read())
                {
                    Producto p = new Producto();

                    p.IdProducto = dr["ID"].ToString();
                    p.NombreProducto = dr["Nombre"].ToString();
                    p.PrecioProducto = Convert.ToSingle(dr["Precio"].ToString());
                    datos.Add(p);
                }

                con.Close();

                return datos;
            }

        //Funcion que genera una id nueva (id n = id[n-1] +1)
            public string generaId() {
                string id="";
                ArrayList datos = new ArrayList();
                int n = 0;
                int max=0; 
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand com = new SqlCommand("select substring(id,3,50) from Producto;", con);
                SqlDataReader dr = com.ExecuteReader();
              
                while (dr.Read())
                {

                  n=Convert.ToInt32(dr[0]);

                  if (n > max)
                      max = n;                               
                }
                max = max + 1;
                id = "id" + max ;
                return id;
            }

            public string getFoto(string id)
            {
                string foto = "";
                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                SqlCommand com = new SqlCommand("Select * from Producto where ID='" + id + "'", con);
                SqlDataReader dr = com.ExecuteReader();

                if (dr.Read())
                {
                    foto = dr["Foto"].ToString();

                }

                con.Close();

                return foto;
            }

            public ArrayList todoProductos()
            {
                ArrayList datos = new ArrayList();
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand com = new SqlCommand("Select * from Producto", con);
                SqlDataReader dr = com.ExecuteReader();

                while (dr.Read())
                {
                    Producto p = new Producto();

                    p.IdProducto = dr["ID"].ToString();
                    p.NombreProducto = dr["Nombre"].ToString();
                    p.PrecioProducto = Convert.ToSingle(dr["Precio"].ToString());
                    datos.Add(p);
                }

                con.Close();

                return datos;
            }

            public bool altaFav(string producto, string usuario)
            {
                bool ap = true;
                try
                {
                    SqlConnection con = new SqlConnection(conexion);
                    con.Open();

                    SqlCommand com = new SqlCommand("UPDATE Usuarios SET producto_favorito='"+producto+"' where email='"+usuario+"'", con);
                    if (com.ExecuteNonQuery() == 1) ap = true;
                    con.Close();
                }
                catch (Exception ex)
                {
                    return false;
                }
                return ap;
            }

            public string getFav(string usuario)
            {
                string fav = "";
                SqlConnection con = new SqlConnection(conexion);
                con.Open();

                SqlCommand com = new SqlCommand("Select * from Usuarios where email='" + usuario + "'", con);
                SqlDataReader dr = com.ExecuteReader();

                if (dr.Read())
                {
                    fav = dr["producto_favorito"].ToString();

                }

                con.Close();

                return fav;
            }

    }
}
