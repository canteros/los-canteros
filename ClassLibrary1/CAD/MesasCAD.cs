using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
 
namespace CanterosLibrary{

    public class MesaCAD
    {
      

        private string conexion;
		 public MesaCAD(){
             conexion = Constantes.dbConexion;
		 }

		//Funcion que retorna los datos de la mesa tomando la id. Si no la encuentra devuelve una mesa vac�a
        public Mesa mesaId(int id){
             
		 	Mesa m = new Mesa();

            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand com = new SqlCommand("Select * from Mesas where NumeroMesa="+id, con);
            SqlDataReader dr = com.ExecuteReader();

            if (dr.Read()) {
                string idAux = dr["NumeroMesa"].ToString();
                m.IdMesa = Convert.ToInt32(idAux);

                string aux = dr["Capacidad"].ToString();
                m.CapacidadMesa = Convert.ToInt32(aux);
            }
            con.Close();
		 	return m;
		 }

        //Funcion para dar de alta una mesa
        //Retorna true si se da de alta correctamente, false en caso contrario
		 public bool altaMesa(Mesa m){
             bool added = false;
             try
             {
                 SqlConnection con = new SqlConnection(conexion);
                 con.Open();
                 SqlCommand com = new SqlCommand("Insert Into Mesas (NumeroMesa,Capacidad) VALUES (" + m.IdMesa + ", " + m.CapacidadMesa + ") ", con);
                 if (com.ExecuteNonQuery() == 1) added = true;
                 con.Close();
             }
             catch (Exception ex) { return false; }
             
              return added;



		 }
        //Funcion para eliminar una mesa
        //Retorna true si se da de baja correctamente, false en caso contrario
		 public bool bajaMesa(int id){
		 	bool deleted = false;
            try
            {
                SqlConnection con = new SqlConnection(conexion);
                con.Open();
                SqlCommand com = new SqlCommand("Delete from Mesas where NumeroMesa=" +id, con);
                if (com.ExecuteNonQuery() == 1) deleted = true;
                con.Close();
            }
            catch (Exception ex) { return false; }



		 	return deleted;


		 }
        //Funcion para modificar la capacidad de una mesa con una id "id" con unos datos "m" (todos los datos se modifican excepto la ID)
        //Retorna true si se modifica correctamente, false en caso contrario
		 public bool modificarMesa(int id, Mesa m){
             bool modificado = false;
             try
             {
                 SqlConnection con = new SqlConnection(conexion);
                 con.Open();
                 SqlCommand com = new SqlCommand("Update Mesas set Capacidad =" + m.CapacidadMesa + "where NumeroMesa="+id, con);
                 if (com.ExecuteNonQuery() == 1) modificado = true;
                 con.Close();


             }
             catch (Exception ex) { return false; }
             return modificado;
		 }

        //Funcion que retorna las mesas existentes y su capacidad entre un rango dado
        //Retorna las mesas, o un ArrayList vac�o si no encuentra ninguna
		 public ArrayList rangoMesas(int min, int max){
             ArrayList mesas = new ArrayList();
             int aux = 0;

             if (max<min) {
                 aux = max;
                 max = min;
                 min = aux;
             }
             SqlConnection con = new SqlConnection(conexion);
             con.Open();
             SqlCommand com = new SqlCommand("Select * from Mesas where Capacidad BETWEEN " + min + "AND "+max, con);
             SqlDataReader dr = com.ExecuteReader();

             while (dr.Read()) {
                 Mesa m = new Mesa();

                    string idAux = dr["NumeroMesa"].ToString();

                       m.IdMesa = Convert.ToInt32(idAux);

                     string cAux = dr["Capacidad"].ToString();

                       m.CapacidadMesa = Convert.ToInt32(cAux);
                       mesas.Add(m);
             }
                
             con.Close();
             return mesas;

		 }

        //Retorna todas las mesas que est�n en la base de datos
         public ArrayList todoMesas()
         {
             ArrayList mesas = new ArrayList();
             SqlConnection con = new SqlConnection(conexion);
             con.Open();
             SqlCommand com = new SqlCommand("Select * from Mesas", con);
             SqlDataReader dr = com.ExecuteReader();

             while (dr.Read())
             {
                 Mesa m = new Mesa();

                 string idAux = dr["NumeroMesa"].ToString();

                 m.IdMesa = Convert.ToInt32(idAux);

                 string cAux = dr["Capacidad"].ToString();

                 m.CapacidadMesa = Convert.ToInt32(cAux);
                 mesas.Add(m);
             }

             con.Close();
             return mesas;

         }

         public int generarNumeroMesa()
         {
             int n = -1;
             SqlConnection con = new SqlConnection(conexion);
             con.Open();
             SqlCommand com = new SqlCommand("Select max(NumeroMesa) from Mesas", con);
             n = Convert.ToInt32(com.ExecuteScalar());
             con.Close();
             return n+1;

         }

	}
}