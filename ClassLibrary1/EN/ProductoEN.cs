﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanterosLibrary
{


    public class Producto
    {
        private string idProducto;
        private string nombreProducto;
        private float precioProducto;

        public Producto()
        {
            idProducto = "";
            nombreProducto = "";
            precioProducto = -1.0F;
        }
        public Producto(string id, string nombre, float precio)
        {

            idProducto = id;
            nombreProducto = nombre;
            precioProducto = precio;

        }

        public Producto(Producto nProducto)
        {

            idProducto = nProducto.idProducto;
            nombreProducto = nProducto.nombreProducto;
            precioProducto = nProducto.precioProducto;
        }



        //Dos objetos son el mismo si sus id son la misma
        public static bool operator ==(Producto p1, Producto p2){
            if(p2.idProducto==p1.idProducto) return true;
            else return false;
        }

        public static bool operator !=(Producto p1, Producto p2)
        {
            if (p2.idProducto != p1.idProducto) return true;
            else return false;
        }


        //!/Todos los nombres de las funciones coinciden con los de sus
        //!/declaraciones, con la primera letra en mayúscula
        public string IdProducto
        {
            get { return idProducto; }
            set { idProducto = value; }
        }
        public string NombreProducto
        {
            get { return nombreProducto; }
            set { nombreProducto = value; }
        }
        public float PrecioProducto
        {
            get { return precioProducto; }
            set { precioProducto = value; }
        }
    }
}
