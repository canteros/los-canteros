﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanterosLibrary
{


    public class Reservas
    {
        private DateTime diaReserva;
        private bool camareroPrivadoReserva;
        private bool fotografoReserva;
        private int mesa;
        private string usuario;

        public Reservas()
        {
            diaReserva = new DateTime(1900,1,1);
            camareroPrivadoReserva = false;
            fotografoReserva = false;
            mesa = 0;
            usuario = "";

        }
        public Reservas(DateTime Dia, bool CamareroPrivado, bool fotografo, int m, string u)
        {
            diaReserva = Dia;
            camareroPrivadoReserva = CamareroPrivado;
            fotografoReserva = fotografo;
            mesa = m;
            usuario = u;

        }


        public Reservas(Reservas iReservas)
        {

            diaReserva = iReservas.diaReserva;
            camareroPrivadoReserva = iReservas.camareroPrivadoReserva;
            fotografoReserva = iReservas.fotografoReserva;
            mesa = iReservas.mesa;
            usuario = iReservas.usuario;
        }





        public DateTime DiaReserva
        {
            get { return diaReserva; }
            set { diaReserva = value; }
        }

        public bool CamareroPrivadoReserva
        {
            get { return camareroPrivadoReserva; }
            set { camareroPrivadoReserva = value; }
        }

        public bool FotorgafoReserva
        {
            get { return fotografoReserva; }
            set { fotografoReserva = value; }
        }

        public int Mesa
        {
            get { return mesa; }
            set {mesa=value;}
        }

        public string Usuario
        {
            get { return usuario; }
            set { usuario=value; }
        }
    }
}
