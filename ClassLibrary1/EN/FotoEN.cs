﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanterosLibrary
{


    public class Foto
    {
        private string path;
        private string name;
        private string description;
        private DateTime uploadDate; //En formato cadena, posiblemente sea mejor usar el formato DateTime

        public Foto()
        {
            path = "";
            name = "";
            description = "";
            uploadDate = new DateTime(1900,1,1);
        }
        public Foto(string path, string name, string des, DateTime fecha)
        {
            this.path = path;
            this.name = name;
            description = des;
            uploadDate = fecha;
        }
        public Foto(Foto foto)
        {
            path = foto.Path;
            name = foto.Name;
            description = foto.Description;
            uploadDate = foto.UploadDate;
        }

        public static bool operator ==(Foto f1, Foto f2)
        {
            if (f1.Path == f2.Path) return true;
            else return false;
        }

        public static bool operator !=(Foto f1, Foto f2)
        {
            if (f1.Path != f2.Path) return true;
            else return false;
        }

        //!/Todos los nombres de las funciones coinciden con los de sus
        //!/declaraciones, con una G delante
        //!/La definicion de los get/set en mayuscula y los return en minúscula/!\

        public string Path
        {
            get { return path; }
            set { path = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public DateTime UploadDate
        {
            get { return uploadDate; }
            set { uploadDate = value; }
        }
    }
}