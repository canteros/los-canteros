using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CanterosLibrary
{


    public class Usuario
    {
        private string email;
        private string nombre;
        private string apellidos;
        private string contrasenya;

        public Usuario()
        {
            email = "";
            nombre = "";
            apellidos = "";
            contrasenya = "";

        }
        public Usuario(string email, string nombre, string apellidos, string contrasenya)
        {


            this.email = email;
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.contrasenya = contrasenya;

        }

        public Usuario(Usuario nUsuario)
        {
            email = nUsuario.email;
            nombre = nUsuario.nombre;
            apellidos = nUsuario.apellidos;
            contrasenya = nUsuario.contrasenya;

        }


        //el email es el cp, lo que diferencia a cada usuario
        public static bool operator ==(Usuario u1, Usuario u2)
        {
            if (u2.email == u1.email)

                return true;
            else return false;
        }

        public static bool operator !=(Usuario u1, Usuario u2)
        {
            if (u2.email != u1.email)

                return true;
            else return false;
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public string Apellidos
        {
            get { return apellidos; }
            set { apellidos = value; }
        }
        public string Contrasenya
        {
            get { return contrasenya; }
            set { contrasenya = value; }
        }



        public static bool validarEmail(string email)
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                { return true; }
                else { return false; }
            }
            else return false;

        }
    }
}
