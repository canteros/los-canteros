﻿
﻿using System;
 using System.Collections.Generic;
 using System.Linq;
 using System.Text;
 using System.Threading.Tasks;
 using System.Collections;
 
 namespace CanterosLibrary
 {
     public class Mesa
     {
         private int idMesa;
         private int capacidadMesa;

 
         public Mesa()
         {
             this.idMesa = 0;
             this.capacidadMesa = 0;
         }
         public Mesa(int idMesa, int capacidadMesa)
         {
             this.idMesa = idMesa;
             this.capacidadMesa = capacidadMesa;
         }
         public Mesa(Mesa m)
         {
             this.idMesa = m.idMesa;
             this.capacidadMesa = m.capacidadMesa;
        }
         public int IdMesa
         {
             get { return idMesa; }
             set { idMesa = value; }
         }
         public int CapacidadMesa{
         	get { return capacidadMesa; }
         	set {capacidadMesa = value; }
         	}
         }
 }
