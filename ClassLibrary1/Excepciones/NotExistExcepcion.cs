﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanterosLibray
{
    class NotExistExcepcion : System.Exception
    {
        private string message;
        private Object valor;
        public NotExistExcepcion(Object valor, string e)
        {
            this.valor = valor;
            this.message = e;
        }
        public Object getValor() { return valor; }
        public string getMessage() { return message; }

    }
}
